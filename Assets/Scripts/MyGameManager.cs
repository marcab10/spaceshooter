﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MyGameManager : MonoBehaviour {
	public Text highscoreText;
	public Text livesText;
	private int Lives;
	private int Highscore;

	private static MyGameManager instance;

	void Awake(){
		if (instance == null) {
			instance = this;
		}
	}
	public static MyGameManager getInstance(){
			return instance;
		}
	// Use this for initialization
	void Start () {
		Lives = 3;
		Highscore = 0;
		highscoreText.text = Highscore.ToString ("D5");

		livesText.text = "x" + Lives.ToString ();
	}
	
	public void LoseLive(){
		Lives--;
		livesText.text = "x" + Lives.ToString ();
	}
		public void AddHighscore(int value){
	}
}
