﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour {
	public float speed;
	private Vector2 axis;
	public Vector2 limits;
	public Propeller propeller;
	public ParticleSystem explosion;
	public GameObject graphics;
	private BoxCollider2D myCollider;
	public AudioSource audio;

	// Update is called once per frame
	void Update () {
		transform.Translate (axis * speed * Time.deltaTime);

		if (transform.position.x > limits.x) {
			transform.position = new Vector3 (limits.x, transform.position.y, transform.position.z);
		}else if (transform.position.x < -limits.x) {
			transform.position = new Vector3 (-limits.x, transform.position.y, transform.position.z);
		}

		if (transform.position.y > limits.y) {
			transform.position = new Vector3 (transform.position.x, limits.y, transform.position.z);
		}else if (transform.position.y < -limits.y) {
			transform.position = new Vector3 (transform.position.x, -limits.y, transform.position.z);
		}

		if (axis.y > 0) {
			propeller.RedFire ();
		} else if (axis.y < 0) {
			propeller.BlueFire ();
		} else {
			propeller.Stop ();
		}
	}

	public void SetAxis(Vector2 currentAxis){
		axis = currentAxis;
	}

	public void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Meteor") {
			Explode ();
		}
	}

	private void Explode(){
		//Perdemos 1 vida
		MyGameManager.getInstance(). loselive ();
		//Quitar grafico
		graphics.SetActive (false);

		//Quitar collider
		myCollider.enabled = false;

		//Lanzar Particula
		explosion.Play ();

		//Play Audio
		audio.Play ();

		//A los 2 segundos reset
		Invoke ("Reset", 2);
	}

	private void Reset(){
		graphics.SetActive (true);
		myCollider.enabled = true;
	}
}

