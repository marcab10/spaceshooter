﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallMeteor : TinyMeteor {

	protected override void Explode(){
		MeteorManager.instance.LaunchMeteor (0, transform.position, new Vector2 (-2, 2), Random.Range(-10,11));
		MeteorManager.instance.LaunchMeteor (0, transform.position, new Vector2 (2, 2), Random.Range(-10,11));
		base.Explode ();
	}
}
